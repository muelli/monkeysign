Monkeysign: OpenPGP Key Exchange for Humans
===========================================

`Monkeysign` is a tool to overhaul the OpenPGP keysigning experience and
bring it closer to something that most primates can understand.

The project makes use of cheap digital cameras and the type of bar code
known as a `QRcode` to provide a human-friendly yet still-secure
keysigning experience.

No more reciting tedious strings of hexadecimal characters. And, you can
build a little rogue's gallery of the people that you have met and
exchanged keys with! (Well, not yet, but it's part of the plan.)

Monkeysign also features a user-friendly commandline tool, similar to
``caff``, to sign OpenPGP keys following the current best practices.

Monkeysign was written by Jerome Charaoui and Antoine Beaupre and is
licensed under GPLv3.

Features
--------

-  commandline and GUI interface
-  GUI supports exchanging fingerprints with qrcodes
-  print your OpenPGP fingerprint on a QRcode
-  key signature done on a separate keyring
-  signature sent in an encrypted email to ensure:

  1. the signee controls the signed email
  2. the signee controls the private key
  3. the signee decides what to do with the signature

-  local ("non-exportable") signatures
-  send through local email server, arbitrary SMTP server or other
   programs

.. For users reading this source file, most documentation is available
   in the `doc/` subdirectory.

For usage instructions, see :doc:`usage` section, for install
instructions, see :doc:`install` section and for support, see the
:doc:`contributing` section.

Similar projects
----------------

-  `OpenKeychain <https://www.openkeychain.org/>`__, a fork of
   `APG <http://www.thialfihar.org/projects/apg/>`__, has support for
   exporting and importing fingerprints in QRcode and NFC. It uses
   similar strings for QRcodes exchanges and is compatible with
   Monkeysign. (`Github
   project <https://github.com/open-keychain/open-keychain>`__)

-  `GPG for Android <https://guardianproject.info/code/gnupg/>`__ (of
   the `Guardian project <https://guardianproject.info/>`__) will import
   public keys in your device's keyring when they are found in QRcodes,
   so it should be able to talk with Monkeysign, but this remains to be
   tested. (`Github
   project <https://github.com/guardianproject/gnupg-for-android>`__)

-  `Gibberbot <https://guardianproject.info/apps/gibber/>`__ (also of
   the `Guardian project <https://guardianproject.info/>`__) can
   exchange OTR fingerprints using QRcodes. (`Github
   project <https://github.com/guardianproject/Gibberbot>`__)

