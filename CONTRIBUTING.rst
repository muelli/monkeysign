Contribute
~~~~~~~~~~

This section explains the various ways users can participate in the
development of Monkeysign, or get support when they find problems.

.. _conduct:

Code of conduct
===============

Our Pledge
----------

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

Our Standards
-------------

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or
  advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

Our Responsibilities
--------------------

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

Scope
-----

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

Enforcement
-----------

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting one of the persons :ref:`listed below <conduct_contacts>` individually. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. Project maintainers are
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

Project maintainers are encouraged to follow the spirit of the `Django
Code of Conduct Enforcement Manual <enforcement_>`_ when receiving
reports.

.. _enforcement: https://www.djangoproject.com/conduct/enforcement-manual/

.. _conduct_contacts:

Contacts
--------

The following people have volunteered to be available to respond to
Code of Conduct reports. They have reviewed existing literature and
agree to follow the aforementioned process in good faith. They also
accept OpenPGP-encrypted email:

* Antoine Beaupré <anarcat@debian.org>
* Daniel Kahn Gillmor <dkg@fifthhorseman.net>

Attribution
-----------

This Code of Conduct is adapted from the `Contributor Covenant <homepage_>`_, version 1.4,
available at `http://contributor-covenant.org/version/1/4 <version_>`_.

.. _homepage: http://contributor-covenant.org
.. _version: http://contributor-covenant.org/version/1/4

Changes
-------

The Code of Conduct was modified to refer to *project maintainers*
instead of *project team* and small paragraph was added to refer to
the Django enforcement manual.

.. note:: We have so far determined that writing an explicit
          enforcement policy is not necessary, considering the
          available literature already available online and the
          relatively small size of the Monkeysign community. This may
          change in the future if the community grows larger.

          This code of conduct was adopted in 2016 by the Monkeysign
          maintainers, see :issue:`54` for more details about the
          discussion.

.. _schedule:

Support schedule
================

First, to know a bit more about the version you are using, understand
that we adhere to `Semantic Versioning <http://semver.org/>`__, which is:

    Given a version number MAJOR.MINOR.PATCH, increment the:

    -  MAJOR version when you make incompatible API changes,
    -  MINOR version when you add functionality in a
       backwards-compatible manner, and
    -  PATCH version when you make backwards-compatible bug fixes.

    Additional labels for pre-release and build metadata are available
    as extensions to the MAJOR.MINOR.PATCH format.

The 2.0.x branch is featured in Debian Jessie and Ubuntu Xenial and is
therefore be maintained for security fixes for the lifetime of those
releases or of any other distribution that picks it up.

Most development and major bug fixes are done directly in the 2.x branch
and published as part of minor releases, which in turn become supported
branches.

Major, API-changing development will happen on the 3.x branch.

Those
`milestones <https://0xacab.org/monkeysphere/monkeysign/milestones>`__
are collaboratively tracked on `0xACAB <https://0xacab.org/monkeysphere/monkeysign/>`_.

Branches status
---------------

Each branch may be in one of those states:

* Development: the development branch is where most of the new
  features are implemented. Consequently, new features and changes may
  inadvertently break things.
* Supported: The branch is supported, but no further development will
  be made on the branch. Only critical issues and security fixes are
  performed.
* Deprecated: users are strongly encouraged to upgrade to later
  versions. No further updates perform except for critical
  security issues.
* Abandoned: the branch is completely abandoned. No further updates
  will ever be performed on the branch, security or otherwise.

====== =========== ====================================================================
Branch Status      Notes
====== =========== ====================================================================
0.x    Abandoned   explicitly unsupported.
1.x    Deprecated  supported until the release of Debian jessie, now only in Debian LTS
2.0.x  Supported   supported until the end of Debian jessie
2.1.x  Abandoned   short lived support branch, superseded by 2.2.x
2.2.x  Supported   supported until the end of Debian stretch
2.x    Development new releases performed here, schedule to be clarified
====== =========== ====================================================================

We try to keep the number of "supported" and "deprecated" branches to
two each, which means it is likely a "deprecated" branch gets
abandoned when a "supported" branch gets "deprecated".

If you are interested in supporting one of those branches beyond the
current state, we would be glad to welcome you on the
team. :doc:`Contact us <support>`!

See also :doc:`history` for more information on past releases.

Documentation
=============

We love documentation!

We maintain the documentation in the Git repository, in `RST`_
format. Documentation can be `edited directly on the website`_ and
built locally with `Sphinx`_ with::

  cd doc ; make html

The Sphinx project has a `good tutorial`_ on RST online. Documentation
is `automatically generated on RTD.io`_.
  
.. _RST: https://en.wikipedia.org/wiki/ReStructuredText
.. _edited directly on the website: https://0xacab.org/monkeysphere/monkeysign/tree/HEAD
.. _Sphinx: http://www.sphinx-doc.org/
.. _good tutorial: http://www.sphinx-doc.org/en/stable/rest.html
.. _automatically generated on RTD.io: https://monkeysign.readthedocs.io/

Translation
===========

Monkeysign is translated using the standard `Gettext
<https://en.wikipedia.org/wiki/Gettetx>`_ translation
system. Translation files are located in the source tree, in the
``po/`` subdirectory and can be edited using `standard translation
tools
<https://www.gnu.org/software/gettext/manual/html_node/Editing.html#Editing>`_
or even a regular text editor. A new translation for your locale can
be created with the ``msginit`` command, see the `gettext manual
<https://www.gnu.org/software/gettext/manual/>`_ for more information
about how to use gettext directly.

You can also use the `Weblate web interface
<https://hosted.weblate.org/projects/monkeysphere/monkeysign/>`_ to
translate Monkeysign directly in your browser, without having to
install any special software. New translations from Weblate need to be
updated in our source tree by hand, so do let us know if you make a
new translation, by filing an issue in our `online issue tracker`_.

.. _online issue tracker: https://0xacab.org/monkeysphere/monkeysign/issues

.. note:: We have chosen `Weblate <http://weblate.org/>`_ instead of
          other solutions because it integrates well with our
          git-based workflow: translations on the site are stored as
          commits in the git repository, and the server is just
          another remote that we can merge directly. It also merges
          our changes automatically and so requires minimal work on
          our part. We have also considered tools like `Transifex
          <https://en.wikipedia.org/wiki/Transifex>`_ (proprietary)
          and `Pootle <https://en.wikipedia.org/wiki/Pootle>`_ (no
          public instance, requires us to run our own).

.. tip:: We encourage our users and developers to `support Weblate's
         development <https://weblate.org/en/donate/>`_. Thank you to
         Weblate's people for hosting our project for free!.

Bug reports
===========

We want you to report bugs you find in Monkeysign. It's an important
part of contributing to a project, and all bug reports will be read and
replied to politely and professionally. See the :doc:`support` section
for more information about troubleshooting and bug reporting.

Bug triage
----------

Bug triage is a very useful contribution as well. You can review the
`issues on 0xACAB <https://0xacab.org/monkeysphere/monkeysign/issues>`__
or in the `Debian BTS for Monkeysign <http://bugs.debian.org/monkeysign>`_. What needs to be done
is, for every issue:

-  try to reproduce the bug, if it is not reproducible, tag it with
   ``unreproducible``
-  if information is missing, tag it with ``moreinfo``
-  if a patch is provided, tag it with ``patch`` and test it
-  if the patch is incomplete, tag it with ``help`` (this is often the
   case when unit tests are missing)
-  if the patch is not working, remove the ``patch`` tag
-  if the patch gets merged into the git repository, tag it with
   ``pending``
-  if the feature request is not within the scope of the project or
   should be refused for other reasons, use the ``wontfix`` tag and
   close the bug (with the ``close`` command or by CC'ing
   ``NNNN-done@bugs.debian.org``)
-  feature requests should have a ``wishlist`` severity

Those directives apply mostly to the Debian BTS, but some tags are also
useful in the 0xACAB site. See also the more `complete directives on how
to use the Debian BTS <https://www.debian.org/Bugs/Developer>`__.

Patches
=======

Patches can be submitted through `merge requests`_ on the `Gitlab
site`_. You will need to `contact the 0xACAB staff`_ to request access
before you can create a fork and a merge request.

.. _Gitlab site: https://0xacab.org/monkeysphere/monkeysign/
.. _merge requests: https://0xacab.org/monkeysphere/monkeysign/merge_requests
.. _contact the 0xACAB staff: https://0xacab.org/riseup/0xacab/issues/new?issue%5Bassignee_id%5D=&issue%5Btitle=fork%20permission%20request&issue%5Bdescription=I%20need%20permission%20to%20fork%20the%20Monkeysign%20repository%20to%20contribute%20to%20the%20project.%20Please%20grant%20me%20fork%20access.%20Thank%20you.

If you prefer old school, offline email systems, you can also use the
Debian BTS, as described above, or send patches to the mailing list for
discussion.

Some guidelines for patches:

* A patch should be a minimal and accurate answer to exactly one
  identified and agreed problem.
* A patch must compile cleanly and pass project self-tests on at least
  the principle target platform.
* A patch commit message must consist of a single short (less than 50
  characters) line stating the a summary of the change, followed by a
  blank line and then a description of the problem being solved and
  its solution, or a reason for the change. Write more information,
  not less, in the commit log.

Maintainers should not merge their own patches unless there is no
response from other maintainers within a reasonable time frame (1-2
days).

.. note:: Those guidelines were inspired by the `Collective Code
          Construct Contract`_. The document was found to be a little
          too complex and hard to read and wasn't adopted in its
          entirety. See `those discussions
          <https://github.com/zeromq/rfc/issues?utf8=%E2%9C%93&q=author%3Aanarcat%20>`_
          for more information.

.. _Collective Code Construct Contract: https://rfc.zeromq.org/spec:42/C4/

Unit tests
==========

Unit tests should be ran before sending patches. They can be ran with
``monkeysign --test`` (starting from Monkeysign 2.1.4, previously it
was ``./test.py`` and only from the source tree).

The tests expect a unicode locale, so if you do not have that configured
already, set one like this, otherwise a part of the test suite will be
skipped::

    export LANG=C.UTF-8
    monkeysign --test

It is possible that some keys used in the tests expire. The built-in
keys do not have specific expiry dates, but some keys are provided to
test some corner cases and *those* keys may have new expiration
dates. Those tests should be skipped when the key expire, but the keys
should eventually be renewed.

To renew the keys, try::

    mkdir ~/.gpg-tmp
    chmod 700 ~/.gpg-tmp
    gpg --homedir ~/.gpg-tmp --import 7B75921E.asc
    gpg --homedir ~/.gpg-tmp --refresh-keys 8DC901CE64146C048AD50FBB792152527B75921E
    gpg --homedir ~/.gpg-tmp --export-options export-minimal --armor --export 8DC901CE64146C048AD50FBB792152527B75921E > 7B75921E.asc

It is also possible the key is just expired and there is no replacement.
In this case the solution is to try and find a similar test case and
replace the key, or simply skip that test.

Debian packaging
================

The Debian package requires backports of ``dh-python`` to operate
properly, otherwise you will get errors like :bts:`839687`::

  LookupError: setuptools-scm was unable to detect version for '/tmp/buildd-...'.

A workaround is to hardcode the version with::

  SETUPTOOLS_SCM_PRETEND_VERSION=x.y.z

Release process
===============

To build a Monkeysign release, you will need to have a few tools
already installed, namely the Python packages ``wheel``,
``setuptools`` and ``setuptools-scm``. We also assume you use the
following Debian packages, although you may be able to work around
those: ``devscripts``, ``git``, ``git-buildpackage``, ``pip`` and
``twine``. In Debian, this should get you started::

  sudo apt install python-wheel python-setuptools python-setuptools-scm devscripts git git-buildpackage python-pip twine

1. make sure tests pass::

     ./scripts/monkeysign --test

2. create release notes with::

     git-dch
     dch -D unstable

3. commit the results::

     git commit -m"prepare new release" -a

4. create a signed and annotated tag::

     git tag -s x.y.z

5. build and test Debian package::

     git-buildpackage
     dpkg -i ../monkeysign_*.deb
     monkeysign --version
     monkeysign --test
     monkeyscan
     dpkg --remove monkeysign

6. build and test Python "wheel"::

     python setup.py bdist_wheel
     pip install dist/*.whl
     monkeysign --version
     monkeysign --test
     monkeyscan
     pip uninstall monkeysign

7. push commits and tags to the git repository::

     git push
     git push --tags

8. publish Python "wheel" on PyPI::

     twine upload dist/*

9. upload Debian package::

     dput ../monkeysign*.changes

10. add announcement on website, IRC channel and mailing list:
    monkeysphere@lists.riseup.net

11. on 0xACAB: close the current `milestone`_, create the next one and
    edit the `release notes on the tag`_

.. _milestone: https://0xacab.org/monkeysphere/monkeysign/milestones
.. _release notes on the tag: https://0xacab.org/monkeysphere/monkeysign/tags
